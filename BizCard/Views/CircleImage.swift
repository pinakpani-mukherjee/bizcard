//
//  CircleImage.swift
//  BizCard
//
//  Created by Pinakpani Mukherjee on 7/14/20.
//  Copyright © 2020 Pinakpani Mukherjee. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var imageName : String
    var body: some View{
        Image(imageName)
        .resizable()
        .frame(width:180, height: 180,alignment: .center)
        .cornerRadius(18)
        .scaledToFit()
        .shadow(radius: 10)
        .clipShape(Circle())
        .overlay(Circle().stroke(Color.white,lineWidth: 3))
    }
}

