//
//  ContentView.swift
//  BizCard
//
//  Created by Pinakpani Mukherjee on 7/13/20.
//  Copyright © 2020 Pinakpani Mukherjee. All rights reserved.
//

import SwiftUI



struct ContentView: View {
    var body: some View {
        VStack(){
            
            CircleImage(imageName: "img_1")
                
            VStack(){
                Text("Pinakpani Mukherjee")
                    .font(.title)
                    .foregroundColor(.white)
                
                Text("mukherjeepinakpani14@gmail.com")
                    .font(.subheadline)
                    .foregroundColor(.white)
                HStack{
                    Image("insta")
                        .resizable()
                        .scaledToFit()
                        .frame(width:20,height: 20)
                        .cornerRadius(5)
                        .shadow(radius: 5)
                    
                    Text("@mukherjee_pinakpani")
                        .font(.subheadline)
                        .foregroundColor(Color.white)
                        .bold()
                        .italic()
                }
                Text("Motto:")
                    .font(.subheadline)
                    .foregroundColor(Color.white)
                    .italic()
                Text("Nil Desperandum!!!")
                    .font(.headline)
                    .bold()
                    .foregroundColor(Color.white)
                
            }
        }.frame(width:360,height: 400)
            .background(Color.purple)
            .cornerRadius(18)
            .shadow(radius: 5)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
